package ck3maptools.config;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ck3maptools.data.WorkFile;
import ck3maptools.logging.Logger;
import ck3maptools.utils.TextUtils;
import lombok.Data;

@Data
public class Config {

	private String gameFolder = "C:/Program Files (x86)/Steam/steamapps/common/Crusader Kings III/game";
	private String modFolder = "";
	private int waterLevel = 17;
	private int heightBlurRadius = 10;

	private static Config singleton;

	private Config() {}

	public static Config getConfig() {
		if (singleton == null) {
			singleton = new Config();
			parseConfig();
		}

		return singleton;
	}

	private static void parseConfig() {

		String path = WorkFile.IN_CONFIG_TXT.getFileName();
		Logger.log("Loading " + path);

		List<String> data = new ArrayList<>();
		try {

			File configFile = new File(path);
			Scanner reader = new Scanner(configFile);
			while (reader.hasNextLine()) {
				data.add(reader.nextLine());
			}
			reader.close();

		} catch (IOException e) {
			Logger.err("config.ini file not found or error parsing it");
			Logger.err(e);
		}

		for (String dataLine : data) {
			if (dataLine.trim().startsWith("#") || dataLine.trim().isEmpty()) {
				// Comment. Ignore.
			} else {
				String[] keyValuePair = dataLine.split("=");

				if (keyValuePair.length != 2) {
					Logger.log("ERROR parsing " + path);
					Logger.log("...not a valid key-value pair : " + dataLine);
				} else {
					try {
						String key = keyValuePair[0].trim();
						String value = keyValuePair[1].trim();

						String setterName = "set" + key.substring(0, 1).toUpperCase() + key.substring(1);
						Class<?> parameterType = null;
						Object valueToInject = null;

						if (value.contains("\"")) {
							valueToInject = value.replaceAll("\"", "");
							parameterType = String.class;
						} else {
							valueToInject = Integer.parseInt(value);
							parameterType = int.class;
						}

						Method setter = Config.class.getMethod(setterName, parameterType);
						setter.invoke(getConfig(), valueToInject);
					} catch (Exception e) {
						Logger.err("ERROR parsing " + path);
						Logger.err("...on key-value pair : " + dataLine);
						Logger.err(e);
					}
				}

			}
		}
	}

	public static void saveConfig() {

		List<String> content = new ArrayList<>();
		content.add("# CK3MapTools Config file ; This file is automatically regenerated.");
		content.add("# Folder of the base game.");
		content.add(String.format("gameFolder=\"%s\"", getConfig().gameFolder));
		content.add("# Folder of your mod. (if any)");
		content.add(String.format("modFolder=\"%s\"", getConfig().modFolder));
		content.add("# Desired sea level. Forces terrains marked as water to stay at this height or below, and other terrains to stay strictly above.");
		content.add(String.format("waterLevel=%d", getConfig().waterLevel));
		content.add("# Max blur radius for heightmap. Higher values make terrain look smoother, but it takes exponentially longer to process.");
		content.add(String.format("heightBlurRadius=%d", getConfig().heightBlurRadius));
		TextUtils.writeTxtFile(WorkFile.IN_CONFIG_TXT, content);

	}

}
