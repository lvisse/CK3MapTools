package ck3maptools.exception;

public class CK3MapToolsException extends RuntimeException {

	private Exception source;
	private CK3MapToolsExceptionType type;

	public CK3MapToolsException(Exception source) { this.source = source; }

	public CK3MapToolsException(CK3MapToolsExceptionType type) { this.type = type; }

}
