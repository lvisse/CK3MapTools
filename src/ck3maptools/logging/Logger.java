package ck3maptools.logging;

import java.io.File;
import java.io.FileWriter;

import ck3maptools.data.WorkFile;
import ck3maptools.utils.FileUtils;
import ck3maptools.utils.TextUtils;

// A basic handmade logger
public class Logger {

	// private static CK3MapToolsUI ui;
	private static String name;
	private static File logFile;
	private static FileWriter writer;

	public static void init(String fileName) {
		File logPath = FileUtils.mkDir(WorkFile.LOGS_FOLDER);
		name = fileName;
		logFile = new File(logPath + "/" + fileName + ".log");
		try {
			writer = new FileWriter(logFile);
		} catch (Exception e) {
			System.out.println("Logger.InitLogger failed " + e.getMessage());
			e.printStackTrace();
		}

		/*
		 * if (ui != null)
		 * ui.resetProgress("Starting "+name+"...");
		 */
	}

	public static void log(String text) { log(text, -1); }

	public static void log(String text, int progress) {
		System.out.println(text);

		try {
			if (writer != null)
				writer.write("[" + TextUtils.getDateString() + "]" + text + "\r\n");
		} catch (Exception e) {
			System.out.println("Logger.log failed " + e.getMessage());
		}

		/*
		 * if (ui != null && progress>=0)
		 * ui.updateProgress(text, progress);
		 */
	}

	public static void err(String text) { log(text, -1); }

	public static void err(Throwable e) {

		if (e == null) {
			Logger.log("NULL exception (wtf?)");
			return;
		}

		Logger.log(e.toString());

		for (StackTraceElement ste : e.getStackTrace()) {
			Logger.log(ste.toString());
		}
	}

	public static void close() {
		try {
			if (writer != null)
				writer.close();
		} catch (Exception e) {
			System.out.println("Logger.close failed " + e.getMessage());
		}

		writer = null;

		/*
		 * if (ui != null)
		 * ui.updateProgress("Finished "+name+"...", 100);
		 */
	}

	/*
	 * public static void registerUI(CK3MapToolsUI ui)
	 * {
	 * Logger.ui = ui;
	 * }
	 */
}
