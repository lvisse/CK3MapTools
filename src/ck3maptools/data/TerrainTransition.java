package ck3maptools.data;

import java.util.Collection;
import java.util.Map;

import ck3maptools.utils.TextUtils;
import lombok.Data;

@Data
public class TerrainTransition {

	private static Map<String, TerrainTransition> terrainTransitions;

	private String name;
	private String terrainSource;
	private String terrainTarget;
	private String terrainReplacement;
	private int radius;
	private boolean bidirectional;

	public static void loadTerrainTransitions() { terrainTransitions = TextUtils.parseInputFileIntoClass(WorkFile.IN_TERRAIN_TRANSITIONS_ODS, TerrainTransition.class); }

	public static Collection<TerrainTransition> getAll() {
		if (terrainTransitions == null)
			loadTerrainTransitions();

		return terrainTransitions.values();
	}
}
