package ck3maptools.data.colormodel;

import java.awt.Color;
import java.awt.image.IndexColorModel;

public enum RiverColorModel {
	RIVER_SOURCE(0, new Color(0, 255, 0)),
	MERGING_RIVER(1, new Color(255, 0, 0)),
	SPLITTING_RIVER(2, new Color(255, 252, 0)),
	WATER(254, new Color(255, 0, 128)),
	LAND(255, new Color(255, 255, 255)),
	RIVER1(3, new Color(0, 225, 255)),
	RIVER2(4, new Color(0, 200, 255)),
	RIVER3(5, new Color(0, 150, 255)),
	RIVER4(6, new Color(0, 100, 255)),
	RIVER5(7, new Color(0, 0, 255)),
	RIVER6(8, new Color(0, 0, 225)),
	RIVER7(9, new Color(0, 0, 200)),
	RIVER8(10, new Color(0, 0, 150)),
	RIVER9(11, new Color(0, 0, 100)),
	RIVER_SOURCE_FROM_MAJOR_RIVER1(12, new Color(0, 85, 0)),
	RIVER_SOURCE_FROM_MAJOR_RIVER2(13, new Color(0, 125, 0)),
	RIVER_SOURCE_FROM_MAJOR_RIVER3(14, new Color(0, 158, 0)),
	RIVER_SOURCE_FROM_MAJOR_RIVER4(15, new Color(24, 206, 0));

	private int index;
	private Color color;

	private RiverColorModel(int index, Color color) {
		this.index = index;
		this.color = color;
	}

	public int getIndex() { return index; }

	public Color getColor() { return color; }

	public int getRGB() { return color.getRGB(); }

	private static IndexColorModel icm = null;

	public static IndexColorModel getIndexColorModel() {
		if (icm != null)
			return icm;

		byte[] redIndex = new byte[256], greenIndex = new byte[256], blueIndex = new byte[256];
		for (RiverColorModel t : RiverColorModel.values()) {
			redIndex[t.getIndex()] = (byte) t.getColor().getRed();
			greenIndex[t.getIndex()] = (byte) t.getColor().getGreen();
			blueIndex[t.getIndex()] = (byte) t.getColor().getBlue();
		}
		icm = new IndexColorModel(8, 256, redIndex, greenIndex, blueIndex);

		return icm;
	}
}
