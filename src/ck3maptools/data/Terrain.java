package ck3maptools.data;

import java.util.Collection;
import java.util.Map;

import ck3maptools.exception.CK3MapToolsException;
import ck3maptools.exception.CK3MapToolsExceptionType;
import ck3maptools.logging.Logger;
import ck3maptools.utils.TextUtils;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Terrain {

	private static Map<String, Terrain> terrains;

	String name;
	private int terrainRGB;
	private int climateRGB;
	private String[] materialsPrimary;
	private String[] materialsSecondary;
	private String[] materialsDetail;
	private String heightType;
	private String gameTerrainType;
	private boolean water;
	private boolean river;
	private boolean coast;
	private float settlementDensity;
	private String winter;
	private String[] mapObjects;
	private int colormapRGB;

	private Integer minHeight = null, maxHeight = null, smoothFactor = null;

	public static void loadTerrains() {
		HeightType.loadHeightTypes();
		terrains = TextUtils.parseInputFileIntoClass(WorkFile.IN_TERRAIN_ODS, Terrain.class);

		for (Terrain t : terrains.values()) {
			HeightType height = HeightType.get(t.getHeightType());
			if (height != null) {
				t.minHeight = height.getMinHeight();
				t.maxHeight = height.getMaxHeight();
				t.smoothFactor = height.getSmoothFactor();
			} else {
				Logger.err("ERROR in Terrain : unable to get height type " + t.getHeightType());
				throw new CK3MapToolsException(CK3MapToolsExceptionType.NULL_HEIGHTS);
			}
		}
	}

	public static Collection<Terrain> getAll() {
		if (terrains == null)
			loadTerrains();

		return terrains.values();
	}

	public static Terrain get(String terrain) {
		if (terrains == null)
			loadTerrains();

		return terrains.get(terrain);
	}

}
