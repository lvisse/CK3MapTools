package ck3maptools.data;

import java.util.Collection;
import java.util.Map;

import ck3maptools.utils.TextUtils;
import lombok.Data;

@Data
public class River {

	private String name;
	private int riverRGB;
	private boolean source;
	private boolean split;
	private boolean merge;
	private int minWidth;
	private int maxWidth;

	private static Map<String, River> rivers;

	public static void loadRivers() { rivers = TextUtils.parseInputFileIntoClass(WorkFile.IN_RIVERS_ODS, River.class); }

	public static Collection<River> getAll() {
		if (rivers == null)
			loadRivers();

		return rivers.values();
	}

	public static River get(String river) {
		if (rivers == null)
			loadRivers();

		return rivers.get(river);
	}
}
