package ck3maptools.data;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import ck3maptools.config.Config;
import ck3maptools.exception.CK3MapToolsException;
import ck3maptools.exception.CK3MapToolsExceptionType;
import ck3maptools.logging.Logger;
import ck3maptools.utils.TextUtils;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Material {

	private static Map<String, Material> materials;

	String name;
	private String maskName;
	private int blurRadius;
	private boolean allowedInWater;
	private String[] mapObjects;

	public static void loadMaterials() {
		// Parse spreadsheet
		materials = TextUtils.parseInputFileIntoClass(WorkFile.IN_MATERIALS_ODS, Material.class);

		File materialSettingsFile;

		// Go get the material.settings from the mod folder
		materialSettingsFile = new File(Config.getConfig().getModFolder() + WorkFile.GAME_GFX_MAP_TERRAIN_MATERIALS_SETTINGS.getFileName());

		if (!materialSettingsFile.exists()) {
			// Go get the material.settings from the game folder instead
			materialSettingsFile = new File(Config.getConfig().getGameFolder() + WorkFile.GAME_GFX_MAP_TERRAIN_MATERIALS_SETTINGS.getFileName());
		}

		if (!materialSettingsFile.exists()) {
			Logger.log(String.format("ERROR : %s could not be found. Check configuration.", materialSettingsFile.getAbsolutePath()));
			throw new CK3MapToolsException(CK3MapToolsExceptionType.GAME_FILE_NOT_FOUND);
		} else {
			parseMaterialSettings(materialSettingsFile);
		}

		materials.remove("default");

	}

	private static void parseMaterialSettings(File materialSettingsFile) {
		try {
			List<String> lines = TextUtils.readTxtFile(materialSettingsFile);

			String materialName = null;
			String maskName = null;

			for (String line : lines) {
				line = line.replace("\t", " ").replace("=", " = ").trim();
				if (!line.startsWith("#")) {
					String[] tokens = line.split(" ");
					String varName = null;
					String operator = null;
					String value = null;
					for (String token : tokens) {
						if (token == null || "".equals(token)) {
							// ignore
						} else if ("{".equals(token)) {
							// Beginning new material ?
							maskName = null;
							materialName = null;
						} else if ("}".equals(token)) {
							// Ending material ?
							if (maskName != null && materialName != null) {
								Material material = materials.get(materialName);
								if (material == null) {
									material = new Material();
									material.setName(materialName);

									Material defaultMaterial = materials.get("default");
									material.setAllowedInWater(defaultMaterial.isAllowedInWater());
									material.setBlurRadius(defaultMaterial.getBlurRadius());
									material.setMapObjects(defaultMaterial.getMapObjects());

									materials.put(materialName, material);

								}
								material.setMaskName(maskName);
							}
						} else {

							if (varName == null) {
								varName = token;
							} else if (operator == null) {
								operator = token;
							} else if (value == null) {
								value = token;
							}

							if ("mask".equals(varName) && "=".equals(operator) && value != null) {
								maskName = value.replaceAll("\"", "").replaceAll(".bmp", ".png");
								// Logger.log("Found maskname :" + maskname);
							}

							if ("id".equals(varName) && "=".equals(operator) && value != null) {
								materialName = value.replaceAll("\"", "");
								// Logger.log("Found material :" + materialName);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			Logger.err("ERROR in parseMaterialSettings");
			Logger.err(e);
			throw new CK3MapToolsException(CK3MapToolsExceptionType.GAME_FILE_PARSING);
		}
	}

	public static Collection<Material> getAll() {
		if (materials == null)
			loadMaterials();

		return materials.values();
	}

	public static Material get(String material) {
		if (materials == null)
			loadMaterials();

		return materials.get(material);
	}

}
