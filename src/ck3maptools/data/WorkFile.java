package ck3maptools.data;

import lombok.Getter;

public enum WorkFile {

	IN_TERRAIN_BMP("./input/terrain.bmp"),
	IN_TERRAIN_FINAL_BMP("./input/terrain_final.png"),
	IN_TERRAIN_ERROR_BMP("./input/terrain_errors.png"),
	IN_TERRAIN_ODS("./input/terrain.ods"),
	IN_TERRAIN_TRANSITIONS_ODS("./input/terrain_transitions.ods"),
	IN_MATERIALS_ODS("./input/materials.ods"),
	IN_HEIGHTS_ODS("./input/heights.ods"),
	IN_CLIMATE_BMP("./input/climate.bmp"),
	IN_CLIMATE_SCRAMBLED_BMP("./input/climate_scrambled.png"),
	IN_SECONDARY_MATERIAL_MAP("./input/secondary_material.png"),
	IN_DETAIL_MATERIAL_MAP("./input/detail_material.png"),
	IN_RIVERS_BMP("./input/rivers.bmp"),
	IN_RIVERS_ERROR_BMP("./input/rivers_errors.png"),
	IN_RIVERS_ODS("./input/rivers.ods"),
	IN_CONFIG_TXT("./input/config.txt"),
	OUT_MAP_DATA_FOLDER("./output/map_data"),
	OUT_HEIGHTMAP_PNG("./output/map_data/heightmap.png"),
	OUT_RIVERS_PNG("./output/map_data/rivers.png"),
	OUT_COMMON_DEFINES_FOLDER("./output/common/defines"),
	OUT_COMMON_DEFINES_TXT("./output/common/defines/99_map_defines.txt"),
	OUT_GFX_MAP_TERRAIN_FOLDER("./output/gfx/map/terrain"),
	OUT_GFX_MAP_TERRAIN_COLORMAP("./output/gfx/map/terrain/colormap.dds"),
	OUT_MAP_OBJECTS_FOLDER("./output/content_source/map_objects/masks"),
	GAME_GFX_MAP_TERRAIN_MATERIALS_SETTINGS("/gfx/map/terrain/materials.settings"),
	GAME_MAP_OBJECTS_GENERATORS_FOLDER("/content_source/map_objects/generators"),
	LOGS_FOLDER("./logs");

	WorkFile(String filename) { this.fileName = filename; }

	@Getter
	private String fileName;

	@Override
	public String toString() { return this.fileName; }

}
