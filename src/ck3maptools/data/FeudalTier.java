package ck3maptools.data;

public enum FeudalTier {
	TIER_BARON,
	TIER_COUNT,
	TIER_DUKE,
	TIER_KING,
	TIER_EMPEROR;
}
