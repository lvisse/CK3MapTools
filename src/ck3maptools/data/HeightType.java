package ck3maptools.data;

import java.util.Collection;
import java.util.Map;

import ck3maptools.utils.TextUtils;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class HeightType {

	private static Map<String, HeightType> heights;

	String name;
	private int minHeight;
	private int maxHeight;
	private int smoothFactor;

	public static void loadHeightTypes() { heights = TextUtils.parseInputFileIntoClass(WorkFile.IN_HEIGHTS_ODS, HeightType.class); }

	public static Collection<HeightType> getAll() {
		if (heights == null)
			loadHeightTypes();

		return heights.values();
	}

	public static HeightType get(String height) {
		if (heights == null)
			loadHeightTypes();

		return heights.get(height);
	}
}
