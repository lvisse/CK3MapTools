package ck3maptools.data.loader;

import java.awt.image.BufferedImage;
import java.io.IOException;

import ck3maptools.data.River;
import ck3maptools.data.Terrain;
import ck3maptools.data.TerrainTransition;
import ck3maptools.data.WorkFile;
import ck3maptools.exception.CK3MapToolsException;
import ck3maptools.exception.CK3MapToolsExceptionType;
import ck3maptools.logging.Logger;
import ck3maptools.utils.CoordUtils;
import ck3maptools.utils.ImageUtils;
import ck3maptools.utils.RGBUtils;
import ck3maptools.utils.RngUtils;
import ck3maptools.utils.ThreadUtils;
import lombok.Data;

// A class to centralize all image loading and parsing, often redundant between 2 programs in
// previous versions
@Data
public class Loader {

	private int sizeX = 0;
	private int sizeY = 0;

	// Input images
	private BufferedImage bufInTerrain; // input/terrain.bmp
	private BufferedImage bufInClimate; // input/climate.bmp
	private BufferedImage bufInRivers; // input/rivers.bmp
	private BufferedImage bufInFinalTerrain;
	private BufferedImage bufInScrambledClimate;

	// terrain.bmp + climate.bmp
	private Terrain[][] terrainArray;
	private Terrain[][] finalTerrainArray;
	boolean anyNullTerrain;
	private TerrainTransition currentTransistion;

	// rivers.bmp
	private River[][] riverArray;

	private static Loader singleton;

	public static Loader getLoader() {
		if (singleton == null) {
			singleton = new Loader();
		}

		return singleton;
	}

	public static void unload() { singleton = null; }

	private Loader() {}

	private void initSize(BufferedImage img) {
		if (img == null)
			return;

		if (sizeX == 0)
			sizeX = img.getWidth();
		if (sizeY == 0)
			sizeY = img.getHeight();
	}

	public void loadTerrain() {
		if (bufInTerrain != null)
			return;

		try {
			bufInTerrain = (BufferedImage) ImageUtils.readInputImage(WorkFile.IN_TERRAIN_BMP.getFileName());
		} catch (IOException e) {
			Logger.err(String.format("ERROR in loadTerrain : %s not found", WorkFile.IN_TERRAIN_BMP.getFileName()));
			bufInTerrain = null;
			throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_NOT_FOUND);
		}

		try {
			bufInClimate = (BufferedImage) ImageUtils.readInputImage(WorkFile.IN_CLIMATE_BMP.getFileName());
		} catch (IOException e) {
			Logger.log(String.format("WARNING in loadTerrain : %s not found", WorkFile.IN_CLIMATE_BMP.getFileName()));
			bufInClimate = null;
			// This is fine though
		}

		initSize(bufInTerrain);

		terrainArray = new Terrain[sizeX][sizeY];

		// Call this before the thread to parse the csv file only once...
		Terrain.loadTerrains();

		ThreadUtils.doThreadedMapPass(this, "loadTerrainThreaded");

		if (anyNullTerrain) {
			BufferedImage bufOutTerrainErrors = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_BYTE_BINARY);
			for (int x = 0; x < sizeX; x++) {
				for (int y = 0; y < sizeY; y++) {
					bufOutTerrainErrors.getRaster().setPixel(x, y, new int[] { terrainArray[x][y] == null ? 1 : 0 });
				}
			}
			try {
				ImageUtils.writeOutputImage(WorkFile.IN_TERRAIN_ERROR_BMP, bufOutTerrainErrors);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw new CK3MapToolsException(CK3MapToolsExceptionType.NULL_TERRAIN);
		}
	}

	public void loadTerrainThreaded(int x, int y) {
		try {
			for (Terrain t : Terrain.getAll()) {
				if (t.getTerrainRGB() == RGBUtils.getColorRGBNoAlpha(bufInTerrain.getRGB(x, y))) {
					if (bufInClimate == null || t.getClimateRGB() == 0) {
						// Terrain matches & no climate specified
						terrainArray[x][y] = t;
					} else if (t.getClimateRGB() == RGBUtils.getColorRGBNoAlpha(bufInClimate.getRGB(x, y))) {
						// Terrain & climate both match
						terrainArray[x][y] = t;
						break;
					}
				}
			}

			if (terrainArray[x][y] == null) {
				Logger.log(String.format("ERROR : NULL terrain at x:%d y:%d", x, y));
				anyNullTerrain = true;
			}
		} catch (Exception e) {
			Logger.err(String.format("ERROR in loadTerrainThreaded at coordinates x:%d y:%d", x, y));
			Logger.err(e);
			throw new CK3MapToolsException(e);
		}
	}

	public void makeTerrainTransitions() throws IOException {
		Logger.log("Making terrain transitions...");
		TerrainTransition.loadTerrainTransitions();

		for (TerrainTransition tt : TerrainTransition.getAll()) {
			finalTerrainArray = new Terrain[sizeX][sizeY];
			currentTransistion = tt;
			ThreadUtils.doThreadedMapPass(this, "makeTerrainTransitionsThreaded");
			terrainArray = finalTerrainArray;
		}
		bufInTerrain = bufInFinalTerrain;
		terrainArray = finalTerrainArray;

		// for debug
		bufInFinalTerrain = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < sizeX; x++) {
			for (int y = 0; y < sizeY; y++) {
				bufInFinalTerrain.setRGB(x, y, terrainArray[x][y].getTerrainRGB());
			}
		}

		ImageUtils.writeOutputImage(WorkFile.IN_TERRAIN_FINAL_BMP, bufInFinalTerrain);

	}

	public void makeTerrainTransitionsThreaded(int x, int y) {
		Terrain t1 = terrainArray[x][y];
		River r1 = riverArray[x][y];

		if (finalTerrainArray[x][y] == null) {
			finalTerrainArray[x][y] = t1;
		}

		if (matchesTerrainTransition(t1, r1, currentTransistion.getTerrainSource())) {
			int radius = currentTransistion.getRadius();
			int radiusSqrd = radius * radius;

			for (int dx = x - radius; dx < x + radius; dx++) {
				for (int dy = y - radius; dy < y + radius; dy++) {
					if (CoordUtils.getDistanceSquared(x, y, dx, dy) <= radiusSqrd) {

						int rx = CoordUtils.inBoundsX(dx);
						int ry = CoordUtils.inBoundsY(dy);

						Terrain t2 = terrainArray[rx][ry];
						River r2 = riverArray[rx][ry];

						if (t1 == t2 && r1 == null && r2 == null) // This is useless and should never happen, don't waste more time
							continue;

						Terrain t3 = currentTransistion.getTerrainReplacement() == null ? null : Terrain.get(currentTransistion.getTerrainReplacement());

						if (matchesTerrainTransition(t2, r2, currentTransistion.getTerrainTarget())) {

							if (t3 != null) {
								finalTerrainArray[rx][ry] = t3;
							} else if (RngUtils.rng(radiusSqrd * 2) == 0) {
								finalTerrainArray[rx][ry] = t1;
							}

							if (currentTransistion.isBidirectional()) {
								if (t3 != null) {
									finalTerrainArray[x][y] = t3;
								} else if (RngUtils.rng(radiusSqrd * 2) == 0) {
									finalTerrainArray[x][y] = t2;
								}
							}
						}
					}
				}
			}
		}
	}

	private boolean matchesTerrainTransition(Terrain t, River r, String string) {
		return string.equals(t.getName()) || (string.equals("any_land") && !t.isWater()) || (string.equals("any_water") && t.isWater())
				|| (string.equals("any_river") && (t.isRiver() || r != null));
	}

	public void scrambleClimate() throws IOException {
		bufInScrambledClimate = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_RGB);
		ThreadUtils.doThreadedMapPass(this, "scrambleClimateThreaded");
		bufInClimate = bufInScrambledClimate;

		// for debug
		ImageUtils.writeOutputImage(WorkFile.IN_CLIMATE_SCRAMBLED_BMP, bufInScrambledClimate);

	}

	public void scrambleClimateThreaded(int x, int y) {
		int radius = 150;
		int radiusSquared = radius * radius;
		int dx = RngUtils.rng(-radius, radius);
		int dy = RngUtils.rng(-radius, radius);

		int rx = CoordUtils.inBoundsX(x + dx);
		int ry = CoordUtils.inBoundsY(y + dy);

		int distanceSqrd = CoordUtils.getDistanceSquared(x, y, rx, ry);

		if (distanceSqrd <= radiusSquared) {
			bufInScrambledClimate.setRGB(x, y, bufInClimate.getRGB(rx, ry));
		} else {
			bufInScrambledClimate.setRGB(x, y, bufInClimate.getRGB(x, y));
		}
	}

	public void loadRivers() {
		if (bufInRivers != null)
			return;

		try {
			bufInRivers = (BufferedImage) ImageUtils.readInputImage(WorkFile.IN_RIVERS_BMP.getFileName());
		} catch (IOException e) {
			Logger.err(String.format("ERROR in loadTerrain : %s not found", WorkFile.IN_RIVERS_BMP.getFileName()));
			bufInRivers = null;
			throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_NOT_FOUND);
		}

		initSize(bufInRivers);

		riverArray = new River[sizeX][sizeY];

		// Call this before the thread to parse the csv file only once...
		River.loadRivers();

		ThreadUtils.doThreadedMapPass(this, "loadRiversThreaded");
	}

	public void loadRiversThreaded(int x, int y) {
		try {
			for (River r : River.getAll()) {
				if (r.getRiverRGB() == RGBUtils.getColorRGBNoAlpha(bufInRivers.getRGB(x, y))) {
					riverArray[x][y] = r;
				}
			}
		} catch (Exception e) {
			Logger.err(String.format("ERROR in loadRiversThreaded at coordinates x:%d y:%d", x, y));
			Logger.err(e);
			throw new CK3MapToolsException(e);
		}
	}

}
