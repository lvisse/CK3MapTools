package ck3maptools.main;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ck3maptools.config.Config;
import ck3maptools.data.River;
import ck3maptools.data.Terrain;
import ck3maptools.data.WorkFile;
import ck3maptools.data.loader.Loader;
import ck3maptools.logging.Logger;
import ck3maptools.utils.CoordUtils;
import ck3maptools.utils.FileUtils;
import ck3maptools.utils.ImageUtils;
import ck3maptools.utils.RngUtils;
import ck3maptools.utils.TextUtils;
import ck3maptools.utils.ThreadUtils;

public class CK3MakeHeightMap implements ICK3MapTool {

	private Loader loader;

	private int[][] heightArray;
	private int[][] heightArrayTemp;
	private int[][] weightArray;
	private int smoothRadius, smoothRadiusSquared;
	private boolean uncap = false, uncapAll = false;
	private int pass;

	public static void main(String[] args) throws Exception {

		CK3MakeHeightMap t = new CK3MakeHeightMap();

		try {
			Logger.init(t.getClass().getSimpleName());
			t.execute();
		} finally {
			Logger.close();
		}

	}

	@Override
	public int execute() throws Exception {

		long ms = System.currentTimeMillis();

		// Parse config.txt
		Config.getConfig();

		// Load the input maps
		loader = Loader.getLoader();
		loader.loadTerrain();
		loader.loadRivers();
		loader.makeTerrainTransitions();

		// Initialize data
		initData();

		// Work magic
		initTerrainHeights();
		for (int i = Config.getConfig().getHeightBlurRadius(); i > 3; i -= Math.max(1, i / 5))
			smoothTerrainHeights(i);
		makeHeightNoise();
		makeErosion();
		uncap = true;
		for (int i = 3; i > 0; i--)
			smoothTerrainHeights(i);

		uncapAll = true; // FINAL-FINAL pass, for real this time
		smoothTerrainHeights(1);

		// Build output & Write Results
		writeOutput();

		Config.saveConfig();

		Logger.log("Done in " + (System.currentTimeMillis() - ms) + "ms", 100);

		return 0;

	}

	private void initData() { heightArray = new int[loader.getSizeX()][loader.getSizeY()]; }

	public void initTerrainHeights() {
		Logger.log("Initializing terrain heights...", 5);
		ThreadUtils.doThreadedMapPass(this, "initTerrainHeightsThreaded");
	}

	public void initTerrainHeightsThreaded(int x, int y) {

		Terrain t = loader.getTerrainArray()[x][y];
		River r = loader.getRiverArray()[x][y];

		// Init Heights
		if (t != null) {
			heightArray[x][y] = t.isWater() || r != null ? t.getMinHeight() * 255 : (t.getMinHeight() + t.getMaxHeight()) * 255 / 2;
		}

	}

	public void smoothTerrainHeights(int radius) {
		Logger.log(String.format("Blurring terrain heights... (radius %d)", radius), 5);

		smoothRadius = radius;
		smoothRadiusSquared = radius * radius;

		long ms1 = System.currentTimeMillis();

		int maxPasses = 0;
		for (Terrain t : Terrain.getAll()) {
			if (t.getSmoothFactor() > maxPasses)
				maxPasses = t.getSmoothFactor();
		}

		for (pass = 0; pass < maxPasses; pass++) {
			heightArrayTemp = new int[loader.getSizeX()][loader.getSizeY()];
			weightArray = new int[loader.getSizeX()][loader.getSizeY()];

			ThreadUtils.doThreadedMapPass(this, "smoothTerrainHeightsThreaded");
			heightArray = heightArrayTemp;
		}

		Logger.log("Blurring done in " + (System.currentTimeMillis() - ms1) + "ms");

	}

	public void smoothTerrainHeightsThreaded(int x, int y) {
		Terrain t = loader.getTerrainArray()[x][y];

		heightArrayTemp[x][y] = adjustTerrainHeight(t, heightArray[x][y]);
		weightArray[x][y] = 1;

		if (pass >= t.getSmoothFactor())
			return;

		for (int dx = Math.max(x - smoothRadius, 0); dx < loader.getSizeX() && dx <= x + smoothRadius; dx++) {
			for (int dy = Math.max(y - smoothRadius, 0); dy < loader.getSizeY() && dy <= y + smoothRadius; dy++) {
				if (CoordUtils.getDistanceSquared(dx, dy, x, y) <= smoothRadiusSquared) {
					heightArrayTemp[x][y] += heightArray[dx][dy];
					weightArray[x][y]++;
				}
			}
		}

		heightArrayTemp[x][y] = heightArrayTemp[x][y] / weightArray[x][y];

		// If capped we respect terrain height boundaries
		if (!uncap) {
			heightArrayTemp[x][y] = adjustTerrainHeight(t, heightArrayTemp[x][y]);
		}
		// Otherwise we don't but still respect water height boundaries, except for the very last pass, to
		// avoid "aliased" coastlines
		else if (!uncapAll) {
			heightArrayTemp[x][y] = adjustTerrainWaterHeight(t, heightArrayTemp[x][y]);
		}
	}

	private int adjustTerrainHeight(Terrain t, int height) { return Math.max(t.getMinHeight() * 255, Math.min(t.getMaxHeight() * 255, height)); }

	private int adjustTerrainWaterHeight(Terrain t, int height) {
		if (t.isCoast())
			return height;
		if (t.isWater())
			return Math.min(height, Config.getConfig().getWaterLevel() * 255);

		return Math.max(height, (Config.getConfig().getWaterLevel() + 1) * 255);
	}

	public void makeHeightNoise() {
		Logger.log("Making noise...", 5);

		long ms2 = System.currentTimeMillis();

		ThreadUtils.doThreadedMapPass(this, "makeHeightNoiseThreaded");

		Logger.log("Noise done in " + (System.currentTimeMillis() - ms2) + "ms");
	}

	public void makeHeightNoiseThreaded(int x, int y) {

		Terrain t = loader.getTerrainArray()[x][y];

		if (t != null && !t.isWater() && RngUtils.rng(10 * t.getSmoothFactor()) == 0) {

			int noNoiseRadius = 3;

			// No noise in close proximity to...
			for (int dx = Math.max(x - noNoiseRadius, 0); dx < loader.getSizeX() && dx <= x + noNoiseRadius; dx++) {
				for (int dy = Math.max(y - noNoiseRadius, 0); dy < loader.getSizeY() && dy <= y + noNoiseRadius; dy++) {
					if (CoordUtils.getDistanceSquared(dx, dy, x, y) <= noNoiseRadius * noNoiseRadius) {
						Terrain t2 = loader.getTerrainArray()[dx][dy];
						River r = loader.getRiverArray()[dx][dy];
						// water
						if (t2 != null && t2.isWater()) {
							return;
						}
						// rivers
						if (r != null) {
							return;
						}
					}
				}
			}

			int maxAmount = (t.getMaxHeight() * 255 - t.getMinHeight() * 255) / 3;
			int amount = RngUtils.rng(t.getMinHeight() * 255 - heightArray[x][y], t.getMaxHeight() * 255 - heightArray[x][y]);

			if (amount > maxAmount)
				amount = maxAmount;
			else if (amount < -maxAmount)
				amount = -maxAmount;

			int noiseRadius = RngUtils.rng(2 + t.getSmoothFactor(), 3 + t.getSmoothFactor());
			int noiseRadiusSquared = noiseRadius * noiseRadius;

			for (int dx = Math.max(x - noiseRadius, 0); dx < loader.getSizeX() && dx <= x + noiseRadius; dx++) {
				for (int dy = Math.max(y - noiseRadius, 0); dy < loader.getSizeY() && dy <= y + noiseRadius; dy++) {
					Terrain t2 = loader.getTerrainArray()[dx][dy];
					if (!t2.isWater()) {
						int distance = CoordUtils.getDistanceSquared(dx, dy, x, y);
						if (distance <= noiseRadiusSquared) {
							heightArray[dx][dy] = heightArray[dx][dy] + (int) (amount / (2 + (double) distance / noiseRadius));
						}
					}
				}
			}

		}
	}

	public void makeErosion() {
		Logger.log("Eroding...", 5);

		long ms3 = System.currentTimeMillis();

		ThreadUtils.doThreadedMapPass(this, "makeErosionThreaded");

		Logger.log("Erosion done in " + (System.currentTimeMillis() - ms3) + "ms");
	}

	public void makeErosionThreaded(int x, int y) {

		// rivers
		River r = loader.getRiverArray()[x][y];
		if (r == null) {
			return;
		}

		Terrain t = loader.getTerrainArray()[x][y];
		heightArray[x][y] = t.getMinHeight();
	}

	private void writeOutput() throws IOException {
		writeHeightMap();

		writeMapDefinesTxt();
	}

	private void writeHeightMap() throws IOException {
		FileUtils.mkDir(WorkFile.OUT_MAP_DATA_FOLDER);
		BufferedImage bufOutHeightmap = new BufferedImage(loader.getSizeX(), loader.getSizeY(), BufferedImage.TYPE_USHORT_GRAY);
		for (int x = 0; x < loader.getSizeX(); x++) {
			for (int y = 0; y < loader.getSizeY(); y++) {
				if (heightArray[x][y] < 0)
					heightArray[x][y] = 0;
				bufOutHeightmap.getRaster().setPixel(x, y, new int[] { heightArray[x][y] });
			}
		}
		ImageUtils.writeOutputImage(WorkFile.OUT_HEIGHTMAP_PNG, bufOutHeightmap);
	}

	private void writeMapDefinesTxt() {
		FileUtils.mkDir(WorkFile.OUT_COMMON_DEFINES_FOLDER);

		List<String> defines = new ArrayList<>();

		defines.add("NJominiMap = {");
		defines.add(String.format("WORLD_EXTENTS_X = %d", loader.getSizeX() - 1));
		defines.add(String.format("WORLD_EXTENTS_Z = %d", loader.getSizeY() - 1));
		defines.add("}");

		TextUtils.writeTxtFile(WorkFile.OUT_COMMON_DEFINES_TXT, defines);
	}
}
