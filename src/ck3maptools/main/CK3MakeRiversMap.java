package ck3maptools.main;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ck3maptools.config.Config;
import ck3maptools.data.River;
import ck3maptools.data.WorkFile;
import ck3maptools.data.colormodel.RiverColorModel;
import ck3maptools.data.loader.Loader;
import ck3maptools.logging.Logger;
import ck3maptools.utils.CoordUtils;
import ck3maptools.utils.ImageUtils;
import ck3maptools.utils.ThreadUtils;

public class CK3MakeRiversMap implements ICK3MapTool {

	private Loader loader;
	private River defaultRiver = null;
	private List<Integer> riversList;
	private List<Integer> iteratedList;
	private byte[][] riverWidth;

	public static void main(String[] args) throws Exception {

		CK3MakeRiversMap t = new CK3MakeRiversMap();

		try {
			Logger.init(t.getClass().getSimpleName());
			t.execute();
		} finally {
			Logger.close();
		}

	}

	@Override
	public int execute() throws Exception {

		long ms = System.currentTimeMillis();

		// Parse config.txt
		Config.getConfig();

		// Load the input maps
		loader = Loader.getLoader();
		loader.loadTerrain();
		loader.loadRivers();

		initData();
		fixRiverEdges();
		findSources();
		reconstructRivers();
		writeOutput();

		Config.saveConfig();

		Logger.log("Done in " + (System.currentTimeMillis() - ms) + "ms", 100);

		return 0;
	}

	private void initData() {
		riverWidth = new byte[loader.getSizeX()][loader.getSizeY()];
		riversList = new ArrayList<>();
		iteratedList = new ArrayList<>();
	}

	public void fixRiverEdges() {
		Logger.log("Fixing river edges...", 5);

		for (River r : River.getAll()) {
			if (!r.isMerge() && !r.isSource() && !r.isSplit()) {
				defaultRiver = r;
				break;
			}
		}

		ThreadUtils.doThreadedMapPass(this, "fixRiverEdgesThreaded");

		BufferedImage bufInRivers = new BufferedImage(loader.getSizeX(), loader.getSizeY(), BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < loader.getSizeX(); x++) {
			for (int y = 0; y < loader.getSizeY(); y++) {
				bufInRivers.setRGB(x, y, loader.getRiverArray()[x][y] == null ? 0 : loader.getRiverArray()[x][y].getRiverRGB());
			}
		}
		try {
			ImageUtils.writeOutputImage(WorkFile.IN_RIVERS_BMP, bufInRivers);
		} catch (IOException e) {
		}
	}

	public void fixRiverEdgesThreaded(int x, int y) {
		if (loader.getRiverArray()[x][y] == null) {
			boolean l, r, u, d, lu, ld, ru, rd;

			try {
				l = loader.getRiverArray()[x - 1][y] != null;
				r = loader.getRiverArray()[x + 1][y] != null;
				u = loader.getRiverArray()[x][y - 1] != null;
				d = loader.getRiverArray()[x][y + 1] != null;

				lu = loader.getRiverArray()[x - 1][y - 1] != null;
				ld = loader.getRiverArray()[x - 1][y + 1] != null;
				ru = loader.getRiverArray()[x + 1][y - 1] != null;
				rd = loader.getRiverArray()[x + 1][y + 1] != null;

				if ((l && u && !lu && !r && !d) || (l && d && !ld && !r && !u) || (r && u && !ru && !l && !d) || (r && d && !rd && !l && !u)) {
					loader.getRiverArray()[x][y] = defaultRiver;
				}

			} catch (Exception e) {

			}
		}
	}

	private void findSources() {
		Logger.log("Finding Sources...");
		for (int x = 0; x < loader.getSizeX(); x++) {
			for (int y = 0; y < loader.getSizeY(); y++) {
				if (loader.getRiverArray()[x][y] != null && loader.getRiverArray()[x][y].isSource()) {
					riversList.add(CoordUtils.getCoordXY(x, y));
				}
			}
		}
	}

	private void reconstructRivers() {
		int step = 0;
		while (!riversList.isEmpty()) {
			step++;
			Logger.log("Reconstructing rivers...Step " + step);
			reconstructRiversStep();
		}
	}

	private void reconstructRiversStep() {
		List<Integer> riversTemp = new ArrayList<>();

		for (Integer source : riversList) {

			boolean foundNext = false;

			do {

				int x = CoordUtils.getCoordX(source), y = CoordUtils.getCoordY(source);
				iteratedList.add(source);

				Integer left = null, right = null, up = null, down = null, next = null;
				List<Integer> neighbours = new ArrayList<>();

				if (x > 0)
					left = CoordUtils.getCoordXY(x - 1, y);
				if (x < loader.getSizeX() - 1)
					right = CoordUtils.getCoordXY(x + 1, y);
				if (y > 0)
					up = CoordUtils.getCoordXY(x, y - 1);
				if (y < loader.getSizeY() - 1)
					down = CoordUtils.getCoordXY(x, y + 1);

				if (left != null && !iteratedList.contains(left))
					neighbours.add(left);
				if (right != null && !iteratedList.contains(right))
					neighbours.add(right);
				if (up != null && !iteratedList.contains(up))
					neighbours.add(up);
				if (down != null && !iteratedList.contains(down))
					neighbours.add(down);

				for (Integer neighbour : neighbours) {

					x = CoordUtils.getCoordX(neighbour);
					y = CoordUtils.getCoordY(neighbour);

					if (loader.getRiverArray()[x][y] != null && !iteratedList.contains(neighbour)) {

						if (loader.getRiverArray()[x][y].isMerge() || loader.getRiverArray()[x][y].isSplit()) {
							riversTemp.add(neighbour);
						} else if (next == null) {
							next = neighbour;
							iteratedList.add(next);
							x = CoordUtils.getCoordX(next);
							y = CoordUtils.getCoordY(next);
						} else {
							Logger.log("River error at coordinates " + x + ";" + y + " : found multiple possible paths");
						}

					}
				}

				foundNext = (next != null);
				source = next;

			} while (foundNext);
		}

		riversList = riversTemp;
	}

	private void writeOutput() {

		BufferedImage bufOutRivers = new BufferedImage(loader.getSizeX(), loader.getSizeY(), BufferedImage.TYPE_BYTE_INDEXED, RiverColorModel.getIndexColorModel());
		BufferedImage bufOutRiversErrors = new BufferedImage(loader.getSizeX(), loader.getSizeY(), BufferedImage.TYPE_BYTE_BINARY);

		for (int x = 0; x < loader.getSizeX(); x++) {
			for (int y = 0; y < loader.getSizeY(); y++) {
				if (loader.getRiverArray()[x][y] == null) {
					if (loader.getTerrainArray()[x][y].isWater()) {
						bufOutRivers.setRGB(x, y, RiverColorModel.WATER.getRGB());
					} else {
						bufOutRivers.setRGB(x, y, RiverColorModel.LAND.getRGB());
					}
				} else if (loader.getRiverArray()[x][y].isSource()) {
					bufOutRivers.setRGB(x, y, RiverColorModel.RIVER_SOURCE.getRGB());
				} else if (loader.getRiverArray()[x][y].isMerge()) {
					bufOutRivers.setRGB(x, y, RiverColorModel.MERGING_RIVER.getRGB());
				} else if (loader.getRiverArray()[x][y].isSplit()) {
					bufOutRivers.setRGB(x, y, RiverColorModel.SPLITTING_RIVER.getRGB());
				} else {
					bufOutRivers.setRGB(x, y, RiverColorModel.RIVER9.getRGB());
				}

				bufOutRiversErrors.getRaster().setPixel(x, y, new int[] { loader.getRiverArray()[x][y] != null && !iteratedList.contains(CoordUtils.getCoordXY(x, y)) ? 1 : 0 });

			}
		}

		try {
			ImageUtils.writeOutputImage(WorkFile.OUT_RIVERS_PNG, bufOutRivers);
			ImageUtils.writeOutputImage(WorkFile.IN_RIVERS_ERROR_BMP, bufOutRiversErrors);
		} catch (IOException e) {
		}
	}
}
