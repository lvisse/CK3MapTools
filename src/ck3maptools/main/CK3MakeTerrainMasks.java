package ck3maptools.main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import ck3maptools.config.Config;
import ck3maptools.data.Material;
import ck3maptools.data.Terrain;
import ck3maptools.data.WorkFile;
import ck3maptools.data.loader.Loader;
import ck3maptools.exception.CK3MapToolsException;
import ck3maptools.exception.CK3MapToolsExceptionType;
import ck3maptools.logging.Logger;
import ck3maptools.utils.CoordUtils;
import ck3maptools.utils.FileUtils;
import ck3maptools.utils.ImageUtils;
import ck3maptools.utils.RGBUtils;
import ck3maptools.utils.RngUtils;
import ck3maptools.utils.ThreadUtils;

public class CK3MakeTerrainMasks implements ICK3MapTool {

	private Loader loader;

	private Material[][] terrainMaterial;

	private boolean[][] materialSecondaryArray;
	private boolean[][] materialDetailArray;
	private byte[][] terrainMaskArray;
	private byte[][] terrainMaskArrayTemp;
	private byte[][] terrainMaterialIntensity;
	private byte[][] mapObjectsIntensity;
	private int[][] colorMapArray;

	private Material currentMaterial;
	private String currentMapObjectType;

	public static void main(String[] args) throws Exception {

		CK3MakeTerrainMasks t = new CK3MakeTerrainMasks();

		try {
			Logger.init(t.getClass().getSimpleName());
			t.execute();
		} finally {
			Logger.close();
		}
	}

	@Override
	public int execute() throws Exception {

		long ms = System.currentTimeMillis();

		// Parse config.txt
		Config.getConfig();

		// Load the input maps
		loader = Loader.getLoader();
		loader.loadTerrain();
		loader.loadRivers();
		loader.makeTerrainTransitions();

		// Load game files and spreadsheets
		Material.loadMaterials();

		// Work magic
		generateDetailMaps();
		initTerrainTextures();

		makeColorMap();
		doMapObjects();

		for (Material material : Material.getAll()) {
			currentMaterial = material;
			initTerrainMask();
			blurTerrainMask();
			writeTerrainMask();
		}

		Config.saveConfig();

		Logger.log("Done in " + (System.currentTimeMillis() - ms) + "ms", 100);

		return 0;

	}

	public void generateDetailMaps() {
		Logger.log("Randomizing terrain materials...", 0);

		materialDetailArray = new boolean[loader.getSizeX()][loader.getSizeY()];
		materialSecondaryArray = new boolean[loader.getSizeX()][loader.getSizeY()];

		for (int loop = 0; loop < loader.getSizeX() * loader.getSizeY() / 150; loop++) {
			int x = RngUtils.rng(loader.getSizeX());
			int y = RngUtils.rng(loader.getSizeY());
			addMaterialDetailArray(x, y);
		}

		for (int loop = 0; loop < loader.getSizeX() * loader.getSizeY() / 50; loop++) {
			int x = RngUtils.rng(loader.getSizeX());
			int y = RngUtils.rng(loader.getSizeY());
			subMaterialDetailArray(x, y);
		}

		for (int loop = 0; loop < loader.getSizeX() * loader.getSizeY() / 50; loop++) {
			int x = RngUtils.rng(loader.getSizeX());
			int y = RngUtils.rng(loader.getSizeY());
			addMaterialSecondaryArray(x, y);
		}

		for (int loop = 0; loop < loader.getSizeX() * loader.getSizeY() / 50; loop++) {
			int x = RngUtils.rng(loader.getSizeX());
			int y = RngUtils.rng(loader.getSizeY());
			subMaterialSecondaryArray(x, y);
		}

		BufferedImage bufOutSecondaryMap = new BufferedImage(loader.getSizeX(), loader.getSizeY(), BufferedImage.TYPE_BYTE_BINARY);
		BufferedImage bufOutDetailMap = new BufferedImage(loader.getSizeX(), loader.getSizeY(), BufferedImage.TYPE_BYTE_BINARY);
		for (int x = 0; x < loader.getSizeX(); x++) {
			for (int y = 0; y < loader.getSizeY(); y++) {
				bufOutSecondaryMap.getRaster().setPixel(x, y, new int[] { materialSecondaryArray[x][y] ? 1 : 0 });
				bufOutDetailMap.getRaster().setPixel(x, y, new int[] { materialDetailArray[x][y] ? 1 : 0 });
			}
		}
		try {
			ImageUtils.writeOutputImage(WorkFile.IN_SECONDARY_MATERIAL_MAP, bufOutSecondaryMap);
			ImageUtils.writeOutputImage(WorkFile.IN_DETAIL_MATERIAL_MAP, bufOutDetailMap);
		} catch (IOException e) {
			Logger.err(e);
			throw new CK3MapToolsException(e);
		}
	}

	private void addMaterialDetailArray(int x, int y) {
		int radius = RngUtils.rng(3, 5);

		for (int dx = CoordUtils.inBoundsX(x - radius); dx <= CoordUtils.inBoundsX(x + radius); dx++) {
			for (int dy = CoordUtils.inBoundsY(y - radius); dy <= CoordUtils.inBoundsY(y + radius); dy++) {
				if (CoordUtils.getDistanceSquared(x, y, dx, dy) <= radius * radius)
					materialDetailArray[dx][dy] = true;
			}
		}
	}

	private void subMaterialDetailArray(int x, int y) {
		int radius = RngUtils.rng(2, 4);

		for (int dx = CoordUtils.inBoundsX(x - radius); dx <= CoordUtils.inBoundsX(x + radius); dx++) {
			for (int dy = CoordUtils.inBoundsY(y - radius); dy <= CoordUtils.inBoundsY(y + radius); dy++) {
				if (CoordUtils.getDistanceSquared(x, y, dx, dy) <= radius * radius)
					materialDetailArray[dx][dy] = false;
			}
		}
	}

	private void addMaterialSecondaryArray(int x, int y) {
		int radius = RngUtils.rng(4, 6);

		for (int dx = CoordUtils.inBoundsX(x - radius); dx <= CoordUtils.inBoundsX(x + radius); dx++) {
			for (int dy = CoordUtils.inBoundsY(y - radius); dy <= CoordUtils.inBoundsY(y + radius); dy++) {
				if (CoordUtils.getDistanceSquared(x, y, dx, dy) <= radius * radius)
					materialSecondaryArray[dx][dy] = true;
			}
		}
	}

	private void subMaterialSecondaryArray(int x, int y) {
		int radius = RngUtils.rng(3, 5);

		for (int dx = CoordUtils.inBoundsX(x - radius); dx <= CoordUtils.inBoundsX(x + radius); dx++) {
			for (int dy = CoordUtils.inBoundsY(y - radius); dy <= CoordUtils.inBoundsY(y + radius); dy++) {
				if (CoordUtils.getDistanceSquared(x, y, dx, dy) <= radius * radius)
					materialSecondaryArray[dx][dy] = false;
			}
		}
	}

	public void initTerrainTextures() {
		Logger.log("Initializing terrain textures...", 0);

		terrainMaterial = new Material[loader.getSizeX()][loader.getSizeY()];
		terrainMaterialIntensity = new byte[loader.getSizeX()][loader.getSizeY()];

		ThreadUtils.doThreadedMapPass(this, "initTerrainTexturesThreaded");
	}

	public void initTerrainTexturesThreaded(int x, int y) {
		try {

			Terrain terrain = loader.getTerrainArray()[x][y];

			// Should we use the details material ?
			boolean detail = materialDetailArray[x][y] && terrain.getMaterialsDetail() != null;

			// Should we use the secondary materials instead of primary ?
			boolean sec = materialSecondaryArray[x][y] && terrain.getMaterialsSecondary() != null;

			int it = 0;
			int pick = 0;
			if (detail) {
				pick = RngUtils.rng(terrain.getMaterialsDetail().length);
			} else if (sec) {
				pick = RngUtils.rng(terrain.getMaterialsSecondary().length);
			} else {
				pick = RngUtils.rng(terrain.getMaterialsPrimary().length);
			}

			String materialData = "";

			if (detail) {
				for (String materialData2 : terrain.getMaterialsDetail()) {
					it++;
					if (pick < it) {
						materialData = materialData2;
						break;
					}
				}
			} else if (sec) {
				for (String materialData2 : terrain.getMaterialsSecondary()) {
					it++;
					if (pick < it) {
						materialData = materialData2;
						break;
					}
				}
			} else {
				for (String materialData2 : terrain.getMaterialsPrimary()) {
					it++;
					if (pick < it) {
						materialData = materialData2;
						break;
					}
				}
			}

			String[] materialDataArray = materialData.split(":");
			String materialName = materialDataArray[0];
			int materialIntensity = 50;
			if (materialDataArray.length > 1) {
				try {
					materialIntensity = Integer.parseInt(materialDataArray[1]);
				} catch (NumberFormatException e) {
					Logger.err(String.format("WARNING: for terrain %s material %s cannot parse intensity value %s", terrain.getName(), materialDataArray[0], materialDataArray[1]));
				}
			}
			terrainMaterial[x][y] = Material.get(materialName);

			if (terrainMaterial[x][y] == null) {
				Logger.err(String.format("ERROR: NULL material at %d %d, material not found : %s", x, y, materialName));
				throw new CK3MapToolsException(CK3MapToolsExceptionType.NULL_MATERIAL);
			}

			terrainMaterialIntensity[x][y] = (byte) (materialIntensity * 255 / 100 - 128);

		} catch (Exception e) {
			Logger.err(String.format("ERROR in initTerrainTexturesThreaded at %d %d", x, y));
			Logger.err(e);
			throw new CK3MapToolsException(e);
		}
	}

	private void initTerrainMask() {
		Logger.log("Initializing terrain mask " + currentMaterial.getName() + " ...", 0);
		terrainMaskArray = new byte[loader.getSizeX()][loader.getSizeY()];
		ThreadUtils.doThreadedMapPass(this, "initTerrainMaskThreaded");
	}

	public void initTerrainMaskThreaded(int x, int y) {
		if (terrainMaterial[x][y] != null && terrainMaterial[x][y].equals(currentMaterial)) {
			terrainMaskArray[x][y] = terrainMaterialIntensity[x][y];
		} else {
			terrainMaskArray[x][y] = -128;
		}
	}

	public void blurTerrainMask() {
		Logger.log("Blurring terrain mask " + currentMaterial.getName() + " ...", 0);

		terrainMaskArrayTemp = new byte[loader.getSizeX()][loader.getSizeY()];

		ThreadUtils.doThreadedMapPass(this, "blurTerrainMaskThreaded");

		terrainMaskArray = terrainMaskArrayTemp;

	}

	public void blurTerrainMaskThreaded(int x, int y) {

		Terrain t = loader.getTerrainArray()[x][y];
		if (t.isWater()) {
			// If not allowed in water, set to 0 and pass.
			if (!currentMaterial.isAllowedInWater()) {
				terrainMaskArrayTemp[x][y] = -128;
				return;
			}
			// Don't bother blurring deep underwater, can't see it most likely.
			else if (t.getMaxHeight() == 0) {
				terrainMaskArrayTemp[x][y] = terrainMaskArray[x][y];
				return;
			}
		}

		int maskAverage = 0;
		int weight = 0;
		int radius = currentMaterial.getBlurRadius();
		int radiusSquared = radius * radius;

		for (int dx = Math.max(x - radius, 0); dx < loader.getSizeX() && dx <= x + radius; dx++) {
			for (int dy = Math.max(y - radius, 0); dy < loader.getSizeY() && dy <= y + radius; dy++) {
				int distance = CoordUtils.getDistanceSquared(dx, dy, x, y);
				if (distance <= radiusSquared) {
					maskAverage += terrainMaskArray[dx][dy];
					weight += 1;
				}
			}
		}

		// Should never happen, but...
		if (weight == 0) {
			terrainMaskArrayTemp[x][y] = -128;
			return;
		}

		terrainMaskArrayTemp[x][y] = (byte) (maskAverage / weight);
	}

	private void writeTerrainMask() throws IOException {
		String maskName = currentMaterial.getMaskName();

		if (maskName != null) {
			FileUtils.mkDir(WorkFile.OUT_GFX_MAP_TERRAIN_FOLDER);
			BufferedImage bufOutTerrainMask = new BufferedImage(loader.getSizeX(), loader.getSizeY(), BufferedImage.TYPE_BYTE_GRAY);
			for (int x = 0; x < loader.getSizeX(); x++) {
				for (int y = 0; y < loader.getSizeY(); y++) {
					bufOutTerrainMask.getRaster().setPixel(x, y, new int[] { terrainMaskArray[x][y] + 128 });
				}
			}
			ImageUtils.writeOutputImage(WorkFile.OUT_GFX_MAP_TERRAIN_FOLDER, maskName, bufOutTerrainMask);
		} else {
			Logger.err("ERROR: no mask name found for material: " + currentMaterial);
			throw new CK3MapToolsException(CK3MapToolsExceptionType.NULL_MATERIAL_MASK_NAME);
		}
	}

	private void doMapObjects() throws IOException {
		Logger.log("Searching for Map Object Types...");

		File gameMapObjectMaskFolder = new File(Config.getConfig().getGameFolder() + WorkFile.GAME_MAP_OBJECTS_GENERATORS_FOLDER);

		if (gameMapObjectMaskFolder.exists()) {

			for (File f : gameMapObjectMaskFolder.listFiles()) {
				if (f.getName().endsWith("_generators.txt")) {
					// TODO: parse it for real

					doMapObjects(f.getName().replace("_generators.txt", "").replace("tree_cypress_01_a", "tree_cypress_01"));
				}
			}
		}
	}

	public void doMapObjects(String name) throws IOException {
		Logger.log("Creating Map Object Type " + name);
		currentMapObjectType = name;

		mapObjectsIntensity = new byte[loader.getSizeX()][loader.getSizeY()];

		ThreadUtils.doThreadedMapPass(this, "doMapObjectsThreaded");

		FileUtils.mkDir(WorkFile.OUT_MAP_OBJECTS_FOLDER);

		BufferedImage bufOutObjectMask = new BufferedImage(loader.getSizeX() / 2, loader.getSizeY() / 2, BufferedImage.TYPE_BYTE_GRAY);
		for (int x = 0; x < loader.getSizeX(); x += 2) {
			for (int y = 0; y < loader.getSizeY(); y += 2) {
				int finalValue = getFinalMapObjectWeight(x, y);

				bufOutObjectMask.getRaster().setPixel(x / 2, y / 2, new int[] { finalValue });
			}
		}

		ImageUtils.writeOutputImage(WorkFile.OUT_MAP_OBJECTS_FOLDER, name + "_mask.png", bufOutObjectMask);
	}

	private int getFinalMapObjectWeight(int x, int y) {
		int totalIntensity = 0;
		int weight = 0;

		for (int dx = x; dx < x + 2; dx++) {
			for (int dy = y; dy < y + 2; dy++) {

				if (loader.getTerrainArray()[dx][dy].isWater()) {
					return 0;
				}

				totalIntensity += mapObjectsIntensity[dx][dy] + 128;
				weight++;
			}
		}

		return weight == 0 ? 0 : (totalIntensity / weight);
	}

	public void doMapObjectsThreaded(int x, int y) {

		mapObjectsIntensity[x][y] = -128;

		if (terrainMaterial[x][y].getMapObjects() != null) {

			for (String mapObjectsData : terrainMaterial[x][y].getMapObjects()) {

				String[] mapObjectsDataArray = mapObjectsData.split(":");

				String mapObjectsDataName = mapObjectsDataArray[0];

				if (mapObjectsDataName.equalsIgnoreCase(currentMapObjectType)) {
					int intensity = 50;
					if (mapObjectsDataArray.length > 1) {
						try {
							intensity = Integer.parseInt(mapObjectsDataArray[1]);
						} catch (NumberFormatException e) {
							Logger.err(String.format("WARNING: for material %s, map objects %s cannot parse intensity value %s", terrainMaterial[x][y], mapObjectsDataArray[0],
									mapObjectsDataArray[1]));
						}
					}

					mapObjectsIntensity[x][y] = (byte) (intensity * 255 / 100 - 128);

					return;
				}
			}
		}
		if (loader.getTerrainArray()[x][y].getMapObjects() != null) {

			for (String mapObjectsData : loader.getTerrainArray()[x][y].getMapObjects()) {

				String[] mapObjectsDataArray = mapObjectsData.split(":");

				String mapObjectsDataName = mapObjectsDataArray[0];

				if (mapObjectsDataName.equalsIgnoreCase(currentMapObjectType)) {
					int intensity = 1;
					if (mapObjectsDataArray.length > 1) {
						try {
							intensity = Integer.parseInt(mapObjectsDataArray[1]);
						} catch (NumberFormatException e) {
							Logger.err(String.format("WARNING: for material %s, map objects %s cannot parse intensity value %s", terrainMaterial[x][y], mapObjectsDataArray[0],
									mapObjectsDataArray[1]));
						}
					}

					if (RngUtils.rng(200) < intensity) {
						mapObjectsIntensity[x][y] = (byte) 127;
					}
				}
			}
		}
	}

	public void makeColorMap() throws IOException {

		Logger.log("Creating color map...");

		colorMapArray = new int[loader.getSizeX()][loader.getSizeY()];
		ThreadUtils.doThreadedMapPass(this, "initColorMapThreaded");

		FileUtils.mkDir(WorkFile.OUT_GFX_MAP_TERRAIN_FOLDER);
		BufferedImage bufOutColorMap = new BufferedImage(loader.getSizeX() / 4, loader.getSizeY() / 4, BufferedImage.TYPE_INT_ARGB);

		for (int x = 0; x < loader.getSizeX(); x += 4) {
			for (int y = 0; y < loader.getSizeY(); y += 4) {

				int finalR = 0;
				int finalG = 0;
				int finalB = 0;
				int weight = 0;

				for (int dx = 0; dx < 4; dx++) {
					for (int dy = 0; dy < 4; dy++) {
						finalR += RGBUtils.getColorR(colorMapArray[x + dx][y + dy]);
						finalG += RGBUtils.getColorG(colorMapArray[x + dx][y + dy]);
						finalB += RGBUtils.getColorB(colorMapArray[x + dx][y + dy]);
						weight++;
					}
				}

				if (weight == 0) {
					bufOutColorMap.setRGB(x / 4, y / 4, RGBUtils.getColorARGB(255, 127, 127, 127));
				} else {
					bufOutColorMap.setRGB(x / 4, y / 4, RGBUtils.getColorARGB(255, finalR / weight, finalG / weight, finalB / weight));
				}
			}
		}

		ImageUtils.writeOutputImage(WorkFile.OUT_GFX_MAP_TERRAIN_COLORMAP, bufOutColorMap);

	}

	public void initColorMapThreaded(int x, int y) {
		Terrain t = loader.getTerrainArray()[x][y];
		colorMapArray[x][y] = t.getColormapRGB() == 0 ? RGBUtils.getColorRGB(127, 127, 127) : t.getColormapRGB();
	}

}
