package ck3maptools.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import ck3maptools.data.loader.Loader;
import ck3maptools.exception.CK3MapToolsException;
import ck3maptools.logging.Logger;

public class ThreadUtils {

	private ThreadUtils() {};

	// Loops on the entire map with 16 threads and calls the method call with params x,y for each
	// x,y point on the map
	public static void doThreadedMapPass(Object caller, String call) throws CK3MapToolsException {
		Loader loader = Loader.getLoader();

		Method methodToCall;
		try {
			methodToCall = caller.getClass().getDeclaredMethod(call, int.class, int.class);
		} catch (NoSuchMethodException | SecurityException e) {
			Logger.err("ERROR calling doThreadedMapPass");
			Logger.err(e);
			throw new CK3MapToolsException(e);
		}
		int x1 = loader.getSizeX() / 4;
		int x2 = loader.getSizeX() / 2;
		int x3 = loader.getSizeX() * 3 / 4;
		int x4 = loader.getSizeX();

		int y1 = loader.getSizeY() / 4;
		int y2 = loader.getSizeY() / 2;
		int y3 = loader.getSizeY() * 3 / 4;
		int y4 = loader.getSizeY();

		Thread t0 = new Thread(() -> doSingleThreadMapPass(0, 0, x1, y1, caller, methodToCall));
		Thread t1 = new Thread(() -> doSingleThreadMapPass(x1, 0, x2, y1, caller, methodToCall));
		Thread t2 = new Thread(() -> doSingleThreadMapPass(x2, 0, x3, y1, caller, methodToCall));
		Thread t3 = new Thread(() -> doSingleThreadMapPass(x3, 0, x4, y1, caller, methodToCall));

		Thread t4 = new Thread(() -> doSingleThreadMapPass(0, y1, x1, y2, caller, methodToCall));
		Thread t5 = new Thread(() -> doSingleThreadMapPass(x1, y1, x2, y2, caller, methodToCall));
		Thread t6 = new Thread(() -> doSingleThreadMapPass(x2, y1, x3, y2, caller, methodToCall));
		Thread t7 = new Thread(() -> doSingleThreadMapPass(x3, y1, x4, y2, caller, methodToCall));

		Thread t8 = new Thread(() -> doSingleThreadMapPass(0, y2, x1, y3, caller, methodToCall));
		Thread t9 = new Thread(() -> doSingleThreadMapPass(x1, y2, x2, y3, caller, methodToCall));
		Thread ta = new Thread(() -> doSingleThreadMapPass(x2, y2, x3, y3, caller, methodToCall));
		Thread tb = new Thread(() -> doSingleThreadMapPass(x3, y2, x4, y3, caller, methodToCall));

		Thread tc = new Thread(() -> doSingleThreadMapPass(0, y3, x1, y4, caller, methodToCall));
		Thread td = new Thread(() -> doSingleThreadMapPass(x1, y3, x2, y4, caller, methodToCall));
		Thread te = new Thread(() -> doSingleThreadMapPass(x2, y3, x3, y4, caller, methodToCall));
		Thread tf = new Thread(() -> doSingleThreadMapPass(x3, y3, x4, y4, caller, methodToCall));

		Thread[] myThreads = { t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, ta, tb, tc, td, te, tf };

		for (Thread t : myThreads)
			t.start();

		boolean stillAlive = true;

		do {
			stillAlive = false;

			for (Thread t : myThreads) {
				if (t.isAlive()) {
					stillAlive = true;

					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// ...
					}
				}
			}

		} while (stillAlive);

	}

	private static void doSingleThreadMapPass(int xstart, int ystart, int xend, int yend, Object caller, Method methodToCall) {

		for (int x = xstart; x < xend; x++) {
			for (int y = ystart; y < yend; y++) {
				try {
					methodToCall.invoke(caller, x, y);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					if (e.getCause() instanceof CK3MapToolsException) {
						throw (CK3MapToolsException) e.getCause();
					} else {
						Logger.err("ERROR in doSingleThreadMapPass when calling " + methodToCall.getName());
						Logger.err(e.getCause());
						throw new CK3MapToolsException(e);
					}
				}
			}
		}
	}
}
