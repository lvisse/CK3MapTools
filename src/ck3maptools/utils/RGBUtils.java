package ck3maptools.utils;

public class RGBUtils {

	private RGBUtils() {};

	public static int getColorR(int color) { return (color & 0xFF0000) >> 16; }

	public static int getColorG(int color) { return (color & 0x00FF00) >> 8; }

	public static int getColorB(int color) { return (color & 0x0000FF); }

	public static int getColorRGB(int r, int g, int b) { return getColorARGB(0, r, g, b); }

	public static int getColorARGB(int a, int r, int g, int b) { return (a << 24) + (r << 16) + (g << 8) + b; }

	public static int getColorRGBNoAlpha(int color) { return (color & 0xFFFFFF); }
}
