package ck3maptools.utils;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ck3maptools.data.WorkFile;
import ck3maptools.logging.Logger;

public class ImageUtils {

	private ImageUtils() {};

	public static RenderedImage readInputImage(String path) throws IOException {
		Logger.log("Loading " + path);
		File input = new File(path);
		return ImageIO.read(input);
	}

	public static void writeOutputImage(WorkFile workFile, String fileName, RenderedImage buffer) throws IOException {
		String fullName = workFile.getFileName();
		if (fileName != null) {
			fullName = workFile.getFileName() + "/" + fileName;
		}
		Logger.log("Writing " + fullName);
		File output = new File(fullName);
		if (fullName.endsWith("bmp")) {
			ImageIO.write(buffer, "bmp", output);
		} else {
			ImageIO.write(buffer, "png", output);
		}
	}

	public static void writeOutputImage(WorkFile workFile, RenderedImage buffer) throws IOException { writeOutputImage(workFile, null, buffer); }
}
