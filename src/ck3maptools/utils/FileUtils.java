package ck3maptools.utils;

import java.io.File;

import ck3maptools.data.WorkFile;

public class FileUtils {

	public static File mkDir(WorkFile workFile) {
		// Setup output dirs if they don't exist
		String path = workFile.getFileName();
		File ret = new File(path);
		if (!ret.exists())
			ret.mkdirs();

		return ret;
	}
}
