package ck3maptools.utils;

import ck3maptools.data.loader.Loader;

public class CoordUtils {

	private CoordUtils() {};

	public static int inBoundsX(int x) {
		if (x < 0)
			return 0;
		else if (x >= Loader.getLoader().getSizeX())
			return Loader.getLoader().getSizeX() - 1;
		else
			return x;
	}

	public static int inBoundsY(int y) {
		if (y < 0)
			return 0;
		else if (y >= Loader.getLoader().getSizeY())
			return Loader.getLoader().getSizeY() - 1;
		else
			return y;
	}

	public static int getDistanceSquared(int x1, int y1, int x2, int y2) { return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2); }

	// New method of storing coordinates :
	// put both X and Y into a single 32-bit integer. Similar to RGB.
	// This is assuming both coordinates are no bigger than 16 bits (65536).
	// Considering the vanilla CK3 map is 8k x 4k this seems somewhat reasonable ?
	// The game engine will probably not handle a much bigger map very well
	public static int getCoordXY(int x, int y) { return (x << 16) + y; }

	public static int getCoordX(int xy) { return (xy & 0xFFFF0000) >> 16; }

	public static int getCoordY(int xy) { return xy & 0x0000FFFF; }
}
