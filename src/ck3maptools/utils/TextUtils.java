package ck3maptools.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import ck3maptools.data.WorkFile;
import ck3maptools.exception.CK3MapToolsException;
import ck3maptools.exception.CK3MapToolsExceptionType;
import ck3maptools.logging.Logger;

public class TextUtils {

	private TextUtils() {};

	public static String getDateString() { return new SimpleDateFormat("yyyy-MM-dd.HH-mm-ss-SSS").format(new Date()); }

	public static void writeTxtFile(WorkFile workFile, List<String> content) {
		File file = new File(workFile.getFileName());
		int indentLevel = 0;

		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
			for (String line : content) {
				if (line.contains("{")) {
					indentLevel++;
				}
				if (line.contains("}")) {
					indentLevel--;
				}
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < indentLevel; i++) {
					sb.append("\t");
				}
				sb.append(line);
				sb.append("\r\n");
				writer.write(sb.toString());
			}
		} catch (IOException e) {
			Logger.err("ERROR: cannot write to " + WorkFile.OUT_COMMON_DEFINES_TXT.getFileName());
			Logger.err(e);
		}
	}

	public static List<String> readTxtFile(File file) {
		List<String> ret = new ArrayList<>();

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
			String line = reader.readLine();
			while (line != null) {
				ret.add(line);
				line = reader.readLine();
			}
		} catch (IOException e) {
			Logger.err("ERROR: cannot read file " + file.getAbsolutePath());
			Logger.err(e);
		}

		return ret;
	}

	// Scans a csv file and returns a List of all rows, where each row is an array of Strings
	public static List<String[]> parseCsvFile(WorkFile csvWorkFile) throws IOException {
		String path = csvWorkFile.getFileName();

		File csvFile = new File(path);

		List<String[]> result = new ArrayList<>();

		Scanner reader = new Scanner(csvFile);

		while (reader.hasNextLine()) {
			result.add(reader.nextLine().split(";"));
		}

		reader.close();

		return result;
	}

	// Scans a LibreOffice Calc (.ods) file and returns a List of all rows, where each row is an array
	// of Strings
	public static List<String[]> parseOdsFile(WorkFile odsWorkFile) throws IOException {
		String path = odsWorkFile.getFileName();

		List<String[]> result = new ArrayList<>();

		File odsFile = new File(path);

		final Sheet sheet = SpreadSheet.createFromFile(odsFile).getSheet(0);

		int column = 0;
		int maxColumns = 0;
		int line = 0;
		int maxLines = 0;
		MutableCell<SpreadSheet> cell;
		do {
			List<String> partialResult = new ArrayList<>();

			do {
				cell = sheet.getCellAt(column, line);
				if (cell != null) {
					partialResult.add(cell.getTextValue());
				}
				column++;
				if (column > maxColumns && sheet.isCellValid(column, line) && !sheet.getCellAt(column, line).isEmpty()) {
					maxColumns = column;
				}
			} while (column <= maxColumns);

			result.add(partialResult.toArray(new String[partialResult.size()]));
			column = 0;
			line++;
			if (line > maxLines && sheet.isCellValid(column, line) && !sheet.getCellAt(column, line).isEmpty()) {
				maxLines++;
			}
		} while (line <= maxLines);

		return result;
	}

	// Scans a WorkFile trying to use either parseOdsFile or parseCsvFile
	private static List<String[]> parseOdsOrCsvFile(WorkFile inputFile) {

		Logger.log("Parsing " + inputFile.getFileName());

		List<String[]> data = new ArrayList<>();

		try {
			if (inputFile.getFileName().endsWith(".ods")) {
				File f = new File(inputFile.getFileName());

				if (f.exists()) {
					data = TextUtils.parseOdsFile(inputFile);
				} else {
					f = new File(inputFile.getFileName().replace(".ods", ".csv"));
					if (f.exists()) {
						data = TextUtils.parseCsvFile(inputFile);
					} else {
						Logger.log("ERROR: file not found " + inputFile.getFileName());
						throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_NOT_FOUND);
					}
				}
			} else {
				data = TextUtils.parseCsvFile(inputFile);
			}
		} catch (IOException e) {
			Logger.log("ERROR: file not found " + inputFile.getFileName());
			throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_NOT_FOUND);
		}
		return data;
	}

	// Scans a ods/csv file where the first line is a list of attributes and the second line is a list
	// of types, and every line after that is an object (of type T) we want to create, and instantiate a
	// map of objects with all the data in our file. First column serves as key.
	public static <T> Map<String, T> parseInputFileIntoClass(WorkFile inputFile, Class<T> returnClass) {

		List<String[]> data = parseOdsOrCsvFile(inputFile);

		Map<String, T> ret = new TreeMap<>();

		String[] headers = null;
		String[] types = null;
		int line = 0;

		for (String[] dataLine : data) {
			line++;

			if (dataLine[0].startsWith("#")) {
				// Comment. Ignore.
			} else if (headers == null) {
				headers = dataLine;
			} else if (types == null) {
				types = dataLine;

				if (types.length != headers.length) {
					Logger.err("ERROR parsing " + inputFile.getFileName());
					Logger.err("...number of columns for types does not match number of columns for headers");
					throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_PARSING);
				}

			} else {
				T newObject;
				String key;
				try {
					parseInputFileCreateObject(returnClass, headers, types, dataLine, ret);
				} catch (CK3MapToolsException e) {
					Logger.err("ERROR parsing " + inputFile.getFileName());
					Logger.err(String.format("...at line %d", line));
					throw e;
				}
			}

		}

		return ret;

	}

	//
	private static <T> void parseInputFileCreateObject(Class<T> returnClass, String[] headers, String[] types, String[] dataLine, Map<String, T> map) {
		T newObject;
		try {
			newObject = returnClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e1) {
			Logger.log("ERROR creating object, could not instanciate " + returnClass.getName());
			throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_PARSING);
		}
		String key = null;

		for (int col = 0; col < dataLine.length && col < headers.length; col++) {
			String word = dataLine[col];
			String header = headers[col];
			String type = types[col];

			Object dataToInject = null;
			Class<?> paramType = null;

			switch (type.toLowerCase()) {
			default:
			case "string":
			case "strings":
				if (word.isEmpty()) {
					continue;
				} else {
					dataToInject = word;
				}
				paramType = String.class;
				break;

			case "int":
				if (word.isEmpty()) {
					continue;
				}
				dataToInject = Integer.parseInt(word);
				paramType = int.class;
				break;

			case "float":
				if (word.isEmpty()) {
					continue;
				}
				dataToInject = Float.parseFloat(word.replace(",", "."));
				paramType = float.class;
				break;

			case "bool":
			case "boolean":
				if (word.isEmpty()) {
					continue;
				}
				dataToInject = Integer.parseInt(word) != 0;
				paramType = boolean.class;
				break;

			case "rgb":
				if (word.isEmpty()) {
					continue;
				}
				String[] colorRGBString = word.split(" ");
				if (colorRGBString.length == 3) {
					int r = Integer.parseInt(colorRGBString[0]);
					int g = Integer.parseInt(colorRGBString[1]);
					int b = Integer.parseInt(colorRGBString[2]);
					dataToInject = RGBUtils.getColorRGB(r, g, b);
				} else {
					continue;
				}
				paramType = int.class;
				break;

			case "comment":
				continue;

			}

			// First column is used as key
			if ("name".equals(header)) {
				key = word;
			}

			// Reflection time
			String setterName = "set" + header.substring(0, 1).toUpperCase() + header.substring(1);
			Method setter;

			// For arrays...
			if (type.equalsIgnoreCase("strings")) {
				String getterName = "get" + header.substring(0, 1).toUpperCase() + header.substring(1);
				Method getter;

				paramType = String[].class;

				try {
					getter = returnClass.getMethod(getterName);
					String[] current = (String[]) getter.invoke(newObject);
					if (current == null) {
						dataToInject = new String[] { (String) dataToInject };
					} else {
						String[] newArray = Arrays.copyOf(current, current.length + 1);
						newArray[current.length] = (String) dataToInject;
						dataToInject = newArray;
					}
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					Logger.err(String.format("ERROR creating object, setting property %s with value %s", header, word));
					Logger.err(e);
					throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_PARSING);
				}
			}

			try {
				setter = returnClass.getMethod(setterName, paramType);
				setter.invoke(newObject, dataToInject);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				Logger.err(String.format("ERROR creating object, setting property %s with value %s", header, word));
				Logger.err(e);
				throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_PARSING);
			}

		}

		// No "name" column for keys, make one up. This won't be useful for retrieving though...
		if (key == null) {
			key = newObject.toString();
		}

		if (key != null) {
			// Logger.log(String.format("Adding Key %s Value %s", key, newObject.toString()));
			if (map.containsKey(key)) {
				Logger.err("Duplicate Key in parseInputFileCreateObject : " + key);
				while (map.containsKey(key)) {
					key = key + RngUtils.rng(10);
				}
			}
			map.put(key, newObject);
		} else {
			Logger.err("ERROR inserting object in map, key is NULL");
			throw new CK3MapToolsException(CK3MapToolsExceptionType.TOOL_FILE_PARSING);
		}
	}
}
