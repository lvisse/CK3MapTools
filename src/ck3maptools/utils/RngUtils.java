package ck3maptools.utils;

import java.util.Random;

public class RngUtils {

	private RngUtils() {};

	private static Random rng = new Random();

	public static int rng(int floor, int ceiling) { return (floor >= ceiling ? floor : floor + rng.nextInt(ceiling - floor)); }

	public static int rng(int ceiling) { return rng(0, ceiling); }
}
